#include <iostream>
#include <ctime>

int main()
{
    const int n = 5; //size of array
    int array[n][n]; 
    
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }

    int check = 22 % n;
    int sum = 0;

    for (int j = 0; j < n; j++)
    {
        sum += array[check][j];
    }
    std::cout << "Sum = " << sum;
}